﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Nikitenko_Mykyta_Project
{
    /// <summary>
    /// Interaction logic for SearchWindow.xaml
    /// </summary>
    public partial class SearchWindow : Window
    {
        private readonly List<DataModel<int, 
                List<Dot>,
                List<Dot>>> _gridData;

        public DataModel<int, 
                   List<Dot>, 
                   List<Dot>> SelectedItem;
        public SearchWindow(List<DataModel<int, 
                List<Dot>, 
                List<Dot>>> gridData)
        {
            _gridData = gridData;
            InitializeComponent();

            
        }

        private void OpenFromFileDataListView_MouseDoubleClick(object sender,
            MouseButtonEventArgs e)
        {
            var item = OpenFromFileDataListView
                .SelectedItem as DataModel<int, string, string>;
            // searching by key
            SelectedItem = _gridData
                .Find(item2 => item2.Key.Equals(item.Key));

            Close();
        }

        private void OpenFromFileDataListView_Loaded(object sender, 
            RoutedEventArgs e)
        {
            foreach (var item in _gridData)
            {
                // displaying data in window
                int key = item.Key;
                string inputData = String.Empty;
                string outputData = String.Empty;
                foreach (var dot in item.InputValues)
                {
                    inputData += $"({dot.X},{dot.Y})\n";
                }

                foreach (var dot in item.OutputValues)
                {
                    outputData += $"({dot.X},{dot.Y})\n";
                }

                OpenFromFileDataListView.
                    Items
                    .Add(new DataModel<int, string, string>(key,
                        inputData,
                        outputData));
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            // index to display
            int? index = null;
            try
            {
                index = Int32.Parse(SearchTextBox.Text);
            }
            catch
            {
                MessageBox.Show("Invalid input", "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error,
                    MessageBoxResult.No);
            }

            OpenFromFileDataListView.Items.Clear();
            OpenFromFileDataListView.Items.Refresh();
            // if invalid input
            if (index == null)
            {
                foreach (var item in _gridData)
                {
                    // Showing values of this key
                    int key = item.Key;
                    string inputData = String.Empty;
                    string outputData = String.Empty;
                    foreach (var dot in item.InputValues)
                    {
                        inputData += $"({dot.X},{dot.Y})\n";
                    }

                    foreach (var dot in item.OutputValues)
                    {
                        outputData += $"({dot.X},{dot.Y})\n";
                    }
                    // adding to list view
                    OpenFromFileDataListView.
                        Items
                        .Add(new DataModel<int, string, string>(key,
                            inputData,
                            outputData));
                }
                return;
            }

            // showing all
            foreach (var item in _gridData.Where(i => i.Key.Equals(index)))
            {
                int key = item.Key;
                string inputData = String.Empty;
                string outputData = String.Empty;
                foreach (var dot in item.InputValues)
                {
                    inputData += $"({dot.X},{dot.Y})\n";
                }

                foreach (var dot in item.OutputValues)
                {
                    outputData += $"({dot.X},{dot.Y})\n";
                }
                // adding to list view
                OpenFromFileDataListView
                    .Items
                    .Add(new DataModel<int, string, string>(key, 
                        inputData,
                        outputData));
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // dynamic windows size changing
            OpenFromFileListView.Columns[1].Width = 
                OpenFromFileListView.Columns[2].Width
               = OpenFromFileDataListView.ActualWidth / 2 - 30;
        }
    }
}
