﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Nikitenko_Mykyta_Project
{
    /// <summary>
    /// Interaction logic for GreetingWindow.xaml
    /// </summary>
    public partial class GreetingWindow : Window
    {
        private readonly DispatcherTimer _dispatcherTimer;
        private int _elapsedSeconds;

        public GreetingWindow()
        {
            InitializeComponent();

	// intiializing fields
            _dispatcherTimer = new DispatcherTimer();
            _dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            _elapsedSeconds = 10;
            CloseButton.Content = String.Format("Close ({0})", _elapsedSeconds);
            _dispatcherTimer.Start();
        }

        private void GotoProjectWindow()
        {
	// executing project window
            new ProjectWindow(10 - _elapsedSeconds).Show();
            Close();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
	
	// counting seconds
            _elapsedSeconds--;
            if (_elapsedSeconds == 0)
            {
                _dispatcherTimer.Stop();
                GotoProjectWindow();
            }
            CloseButton.Content = String.Format("Close ({0})", _elapsedSeconds);
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            _dispatcherTimer.Stop();
            GotoProjectWindow();
        }
    }
}
