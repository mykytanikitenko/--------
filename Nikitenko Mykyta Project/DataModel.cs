﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikitenko_Mykyta_Project
{
    public class DataModel<TKey,TInputValues,TOutputValues>
    {
        public TKey Key { get; set; }
        public TInputValues InputValues { get; set; }
        public TOutputValues OutputValues { get; set; }
        public DataModel(TKey key, TInputValues inputValues, TOutputValues outputValues)
        {
            Key = key;
            InputValues = inputValues;
            OutputValues = outputValues;
        }
    }
}
