﻿using System.Collections.Generic;

namespace Nikitenko_Mykyta_Project
{
    public static class ListExtensions
    {
        public static List<T> Clone<T>(this List<T> oldList)
        {
            var newList = new List<T>(oldList.Capacity);
            newList.AddRange(oldList);
            return newList;
        }
    }
}