﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikitenko_Mykyta_Project
{
    public class Dot
    {
        public int X { get; set; }
        public int Y { get; set; }
        public Dot(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
