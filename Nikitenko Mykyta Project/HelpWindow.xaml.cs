﻿using System.Windows;

namespace Nikitenko_Mykyta_Project
{
    /// <summary>
    /// Interaction logic for HelpWindow.xaml
    /// </summary>
    public partial class HelpWindow : Window
    {
        public HelpWindow()
        {
            InitializeComponent();

	// show free space of drivesr
            var process = new System.Diagnostics.Process();

            process.StartInfo.FileName = "wmic";

            process.StartInfo.Arguments = "logicaldisk list brief";

            process.StartInfo.UseShellExecute = false;

            process.StartInfo.RedirectStandardOutput = true;

            process.StartInfo.CreateNoWindow = true;

            process.Start();

	// reading output
            string output = process.StandardOutput.ReadToEnd();

            process.WaitForExit();

            DiskSpaceDataTextBlock.Text += output;
        }
    }
}
