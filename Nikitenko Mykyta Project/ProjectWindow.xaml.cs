﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;

namespace Nikitenko_Mykyta_Project
{
    /// <summary>
    /// Interaction logic for ProjectWindow.xaml
    /// </summary>
    public partial class ProjectWindow : Window
    {
        private readonly DispatcherTimer _openedProgramTimer;
        private readonly DateTime _programOpenedDateTime;
        private readonly DispatcherTimer _waitInputDataTimer;
        private int _elapsedSeconds;
        private List<Dot> _inputData;
        private List<Dot> _outputData;
        private int _previvousState;
        private List<Dot> _saveStateInputData;
        private List<Dot> _saveStateOtputData;
        private string _workingFilePath;

        public ProjectWindow(int secondsFromPrevivousWindow)
        {
            InitializeComponent();
		
	// wait input data timer
            _waitInputDataTimer = new DispatcherTimer();
            _waitInputDataTimer
                .Tick += new EventHandler(waitInputDataTimer_Tick);

            _waitInputDataTimer
                .Interval = new TimeSpan(0, 0, 1);

            _openedProgramTimer = new DispatcherTimer();
            _openedProgramTimer
                .Tick += new EventHandler(openedProgramTimer_Tick);

            _openedProgramTimer.Interval = new TimeSpan(0, 0, 1);

	// initializing list for containing input and output data
            _inputData = new List<Dot>();
            _outputData = new List<Dot>();

            _previvousState = _inputData.Count;

            
            _openedProgramTimer.Start();

	// geting datetime state of current moment
            _programOpenedDateTime = DateTime.Now - new TimeSpan(0,0,secondsFromPrevivousWindow);

            durationTextBlock.Text = (DateTime.Now - _programOpenedDateTime)
                .ToString(@"hh\:mm\:ss");

            _workingFilePath = null;

            Title = "Nikitenko Mykyta's Project";

            SaveState();

            Closing += new CancelEventHandler(OnClosing_EventHandler);
        }

        private void OnClosing_EventHandler(object sender, CancelEventArgs e)
        {
            // if state changes
            if (IsStateChanged())
            {
                var result = MessageBox
                    .Show("You have unsaved changes." +
                    " Do you want to save them before exit?",
                    "Warning",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);

                // getting result
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        SaveMenuItem_Click();
                        return;
                        break;

                    case MessageBoxResult.No:
                        return;
                        break;
                }
            }
            else
            {
                var result = MessageBox.Show("Are you shure?",
                    "Warning",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);

                switch (result)
                {
                    case MessageBoxResult.Yes:
                        return;
                        break;

                    case MessageBoxResult.No:
                        return;
                }
            }

        }
        private int elapsedSeconds
        {
            get
            {
                return _elapsedSeconds;
            }
            set
            {
                _elapsedSeconds = value;

                if (elapsedSeconds == 0)
                {
                    if (!IsStateOfInputChanged())
                    {
			// blocking buttons and checking input state
                        BlockButtons();
                        _waitInputDataTimer.Stop();
                        StartButton.Content = "Start";
                        if (_inputData.Any())
                        {
                            MessageBox
                                .Show("You should enter data in 5 seconds",
                                "Warning",
                                MessageBoxButton.OK,
                                MessageBoxImage.Warning);
                        }
                    }
                    else
                    {
                        elapsedSeconds = 5;
                    }
                }
                else
                {
		// end timer if input detected
                    if (IsStateOfInputChanged())
                    {
                        UnblockButtons();
                        _waitInputDataTimer.Stop();
                        StartButton.Content = "Start";
                        return;
                    }
                    else
                    {
                        StartButton.Content = String.Format("Start ({0})",
                        _elapsedSeconds);
                    }
                }
                _previvousState = _inputData.Count();
            }
        }
        private string workingFilePath
        {
            get
            {
                return _workingFilePath;
            }
            set
            {
                _workingFilePath = value;
                if(String.IsNullOrEmpty(workingFilePath))
                {
                    return;
                }
		// creting file if it's not exist
                if (!File.Exists(workingFilePath))
                {
                    using (var fileStream = File.Create(workingFilePath))
                    {
                        var info = new UTF8Encoding(true)
                            .GetBytes(String.Empty);

                        fileStream.Write(info, 0, info.Length);
                    }
                }

		// setting title for working file path
                Title = "Nikitenko Mykyta's Project (" + workingFilePath + ")";
                SaveState();
            }
        }
        private void AboutMenuItem_Click(object sender, RoutedEventArgs e)
        {
            new HelpWindow().Show();
        }

        private void Add(int x, int y, bool showMessageBox = false)
        {
	// Function for adding dots values
            var newDot = new Dot(x, y);
            if (_inputData.Any(dot => dot.X == x))
            {
                if(showMessageBox)
                    MessageBox.Show("You have already entered value for this X coordinate",
                        "Invalid input",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            _inputData.Add(newDot);

	// sorting input data
            _inputData.Sort((xx, yy) => xx.X - yy.X);
            inputListView.Items.Clear();
            inputListView.Items.Refresh();

            foreach (var item in _inputData)
            {
                inputListView.Items.Add(item);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
	// limit for 200 values
            if(inputListView.Items.Count > 200)
            {

                MessageBox.Show("You cant input more than 200 values",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;

            }
	// parsing input
            int x,y;
            try
            {
                x = Int32.Parse(InputX.Text);
                y = Int32.Parse(InputY.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, 
                    "Invalid input", 
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
	// if all success, adding values to window
            Add(x, y, showMessageBox: true);
            InputX.Text = String.Empty;
            InputY.Text = String.Empty;
        }

        private void BeginNew()
        {
	// clearing input state
            _inputData = new List<Dot>();
            _outputData = new List<Dot>();

            _previvousState = _inputData.Count();

	// clearing working file path
            _workingFilePath = null;
            inputListView.Items.Clear();
            inputListView.Items.Refresh();

            outputListView.Items.Clear();
            outputListView.Items.Refresh();

            SaveState();
        }

        private void BlockButtons()
        {
	// unenabling buttons
            AddButton.IsEnabled = false;
            GenerateRandomButton.IsEnabled = false;
            FindButton.IsEnabled = false;

            StartButton.IsEnabled = true;
        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
	// if state changes
            if (IsStateChanged())
            {
                var result = MessageBox
                    .Show("You have unsaved changes." +
                    " Do you want to save them before exit?",
                    "Warning",
                    MessageBoxButton.YesNoCancel,
                    MessageBoxImage.Warning);

		// getting result
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        SaveMenuItem_Click();
                        Close();
                        break;

                    case MessageBoxResult.No:
                        Close();
                        break;

                    case MessageBoxResult.Cancel:
                        return;
                }
            }
            else
            {
                var result = MessageBox.Show("Are you shure?",
                    "Warning",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);

                switch (result)
                {
                    case MessageBoxResult.Yes:
                        Close();
                        break;

                    case MessageBoxResult.No:
                        return;
                }
            }
        }

        private void FindButton_Click(object sender, RoutedEventArgs e)
        {
	// Dots can't be less, then 3
            if (_inputData.Count() < 3)
            {
                MessageBox.Show("Too many dots", "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
            if (outputListView.Items.Count > 0)
            {
                outputListView.Items.Clear();
                _outputData.Clear();
            }

            var extremums = new List<int>();

	// necessery algorythm for searcing dots
            for(int index = 1; index < inputListView.Items.Count - 1; index++)
            {
                if( ((Dot) inputListView.Items[index-1]).Y <
                        ((Dot)inputListView.Items[index]).Y &&
                   ((Dot) inputListView.Items[index]).Y > 
                    ((Dot) inputListView.Items[index + 1]).Y)
                {
                    extremums.Add(index);
                }
            }

	// adding them to window
            foreach(var index in extremums)
            {
                outputListView.Items.Add(inputListView.Items[index]);
                _outputData.Add((Dot) inputListView.Items[index]);

            }
	// if any dots found
            if (extremums.Count() == 0)
            {
                MessageBox.Show("Any dots found",
                    "Warning",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }
        }
        private void GenerateRandomButton_Click(object sender,
            RoutedEventArgs e)
        {
	// generating random values
            var randomNumber = new Random();
            for (int x = -100; x <= 100; x++)
            {
                Add(x, randomNumber.Next(-100, 100));
            }
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
	// dynamically chaning window size
            inputGridView.Columns[0].Width = inputGridView.Columns[1].Width
                = inputListView.ActualWidth / 2 - 14;

            outputGridView.Columns[0].Width = outputGridView.Columns[1].Width
                = outputListView.ActualWidth / 2 - 14;
        }

        private bool IsStateChanged()
        {
	// checking for changgd state
            return _saveStateInputData.Count() != _inputData.Count() ||
                _saveStateOtputData.Count() != _outputData.Count();
        }

        private bool IsStateOfInputChanged()
        {
	// cheking state of input
            return _previvousState != _inputData.Count();
        }

        private void newMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (IsStateChanged())
            {
		// save usaved changes
                var result = MessageBox
                    .Show("You have unsaved changes. Do you want to save them?",
                    "Warning",
                    MessageBoxButton.YesNoCancel
                    , MessageBoxImage.Warning);

                switch (result)
                {
                    case MessageBoxResult.Yes:
                        SaveMenuItem_Click();
                        BeginNew();
                        break;

                    case MessageBoxResult.No:
                        BeginNew();
                        break;

                    case MessageBoxResult.Cancel:
                        return;
                }
            }
            else
            {
                BeginNew();
            }
        }

        private void openedProgramTimer_Tick(object sender, EventArgs e)
        {
	// timer ticking and seting duration block
            _programOpenedDateTime.AddSeconds(1);
            durationTextBlock.Text = (DateTime.Now - _programOpenedDateTime)
                .ToString(@"hh\:mm\:ss");
        }

        private void OpenFromFile()
        {
            var openFileDialog = new OpenFileDialog();

            openFileDialog.DefaultExt = ".maf";
            openFileDialog.Filter = "Mykyta's another format (.maf)|*.maf";
            openFileDialog.CheckFileExists = true;

            bool? result = openFileDialog.ShowDialog();

            if (result == true)
            {
		// reading file content
                workingFilePath = openFileDialog.FileName;
                string dataFromFile = File.ReadAllText(workingFilePath,
                        Encoding.UTF8);

                List<DataModel<int,
                                List<Dot>,
                                List<Dot>>> loadedData;
                try
                {
		// trying to serialize it
                    loadedData = JsonConvert
                    .DeserializeObject<List<DataModel<int,
                                                   List<Dot>,
                                                   List<Dot>>>>
                                                    (dataFromFile);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message,
                    "Invalid file content",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
		// showing search window
                var searchWindow = new SearchWindow(loadedData);
                searchWindow.ShowDialog();
                if(searchWindow.SelectedItem == null)
                {
                    return;
                }
		// geting selected item
                _inputData = searchWindow.SelectedItem.InputValues;
                _outputData = searchWindow.SelectedItem.OutputValues;

                inputListView.Items.Clear();
                inputListView.Items.Refresh();

                outputListView.Items.Clear();
                outputListView.Items.Refresh();

		// adding them to window
                foreach (var item in _inputData)
                {
                    inputListView.Items.Add(item);
                }

                foreach (var item in _outputData)
                {
                    outputListView.Items.Add(item);
                }
            }
        }

        private void openMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (IsStateChanged())
            {
		// checking for unsaved changes
                var result = MessageBox
                    .Show("You have unsaved changes. Do you want to save them?",
                    "Warning",
                    MessageBoxButton.YesNoCancel,
                    MessageBoxImage.Warning);

                switch (result)
                {
                    case MessageBoxResult.Yes:
			// save menu item  click
                        SaveMenuItem_Click();
                        OpenFromFile();
                        break;

                    case MessageBoxResult.No:
			// opening from file
                        OpenFromFile();
                        break;

                    case MessageBoxResult.Cancel:
                        return;
                }
            }
            else
            {
                OpenFromFile();
            }
        }

        private void SaveAsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SaveAsMenuItem_Click();
        }

        private void SaveAsMenuItem_Click()
        {
            var openFileDialog = new OpenFileDialog();

            openFileDialog.DefaultExt = ".maf";
            openFileDialog.Filter = "Mykyta's another format (.maf)|*.maf";
            openFileDialog.CheckFileExists = false;

            bool? result = openFileDialog.ShowDialog();
            workingFilePath = openFileDialog.FileName;
            if(workingFilePath == null)
            {
                return;
            }
            if (result == true && WorkingFileIsEmpty())
            {
		// initializing index key
                int indexKey = 1;
                
                var dataToSave =
                    new List<DataModel<int,
                            List<Dot>,
                            List<Dot>>>();

		// adding to data which we want to save
                dataToSave.Add(new DataModel<int,
                                          List<Dot>,
                                          List<Dot>>(indexKey,
                                            _inputData,
                                            _outputData));

		// serializing 
                string data = JsonConvert.SerializeObject(dataToSave,
                        Formatting.Indented);
                File.WriteAllText(workingFilePath, data);
            }
            else if(result == true && !WorkingFileIsEmpty())
            {
                string dataFromFile = File.ReadAllText(workingFilePath,
                        Encoding.UTF8);
		
		// serializing already saved data
                var loadedData = JsonConvert
                    .DeserializeObject<List<DataModel<int,
                                                   List<Dot>,
                                                   List<Dot>>>>
                                                    (dataFromFile);
		// incrementing index key
                var indexKey = loadedData.Count() + 1;
                loadedData.Add(new DataModel<int,
                                          List<Dot>,
                                          List<Dot>>(indexKey,
                                            _inputData,
                                            _outputData));
                var dataToSave = loadedData;
                string data = JsonConvert.SerializeObject(dataToSave,
                        Formatting.Indented);

		// writing it to file;
                File.WriteAllText(workingFilePath, data);
            }
        }

        private void SaveMenuItem_Click()
        {
            if (workingFilePath == null)
            {
                SaveAsMenuItem_Click();
                return;
            }
            if(IsStateChanged() && !WorkingFileIsEmpty())
            {
		// reading data from file
                string dataFromFile = File.ReadAllText(workingFilePath,
                        Encoding.UTF8);

		// Deserializing it
                var loadedData = JsonConvert
                    .DeserializeObject<List<DataModel<int,
                                                   List<Dot>,
                                                   List<Dot>>>>
                                                    (dataFromFile);

		// incrementing index key
                var indexKey = loadedData.Count() + 1;
                loadedData.Add(new DataModel<int,
                                          List<Dot>,
                                          List<Dot>>(indexKey,
                                            _inputData,
                                            _outputData));
                var dataToSave = loadedData;
		
		// serializing it againt
                string data = JsonConvert.SerializeObject(dataToSave,
                        Formatting.Indented);

                File.WriteAllText(workingFilePath, data);
            }
            else if (IsStateChanged() && WorkingFileIsEmpty())
            {
		// initializing index key
                int indexKey = 1;
                var dataToSave = new DataModel<int,
                                          List<Dot>,
                                          List<Dot>>(indexKey,
                                            _inputData,
                                            _outputData);
		
		// serializing
                string data = JsonConvert.SerializeObject(dataToSave);
                File.WriteAllText(workingFilePath, data);
            }
            SaveState();
        }  

        private void SaveMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SaveMenuItem_Click();
        }

        private void SaveState()
        {
            _saveStateInputData = _inputData.Clone();
            _saveStateOtputData = _outputData.Clone();
        }
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
	// unblocking buttons
            UnblockButtons();
            _previvousState = _inputData.Count();
            _waitInputDataTimer.Start();

	// setiing elapsed seconds
            elapsedSeconds = 5;
            StartButton.Content = String.Format("Start ({0})", elapsedSeconds);
        }

        private void UnblockButtons()
        {
            AddButton.IsEnabled = true;
            GenerateRandomButton.IsEnabled = true;
            FindButton.IsEnabled = true;

            StartButton.IsEnabled = false;
        }

        private void waitInputDataTimer_Tick(object sender, EventArgs e)
        {
            if (IsStateOfInputChanged())
            {
                elapsedSeconds = 5;
                return;
            }
            elapsedSeconds--;
        }

        private bool WorkingFileIsEmpty()
        {
            var info = new FileInfo(workingFilePath);
            return !info.Exists || info.Length == 0;
        }
    }
}
